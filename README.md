# XRP Extension

A small proof of concept firefox extension to allow sites to support logging in via XRP.

![login](./assets/login.png)
![account](./assets/account.png)

## Get started

Install the dependencies...

```bash
git clone https://gitlab.com/astronft/xrp-extension.git
cd xrp-extension
npm install
```

...then start [Webpack](https://webpack.js.org/):

```bash
npm run dev
```

Navigate to [localhost:8080](http://localhost:8080). You should see your app running. Edit a component file in `src`, save it, and reload the page to see your changes.

If you're using [Visual Studio Code](https://code.visualstudio.com/) we recommend installing the official extension [Svelte for VS Code](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode). If you are using other editors you may need to install a plugin in order to get syntax highlighting and intellisense.

## Building and running in production mode

To create an optimised version of the app:

```bash
npm run build
```

You can run the newly built app with `npm run start`. This uses [sirv](https://github.com/lukeed/sirv), which is included in your package.json's `dependencies` so that the app will work when you deploy to platforms like [Heroku](https://heroku.com).

## Installing as a Firefox extension

1. Run `npm run build`.
2. Go to `about:debugging#/runtime/this-firefox`
3. Click "Load Temporary Add-on..", navigate to the `xrp-extension/public` folder and select the `index.html` file.
4. Login with password `123`.
