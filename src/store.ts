import type { FormattedPaymentTransaction } from "ripple-lib/dist/npm/transaction/types";
import { derived, writable } from "svelte/store";

const sortTransactions = (transactions: FormattedPaymentTransaction[]) => {
    // descending, latest first
    transactions.sort((a, b) => Date.parse(b.outcome.timestamp) - Date.parse(a.outcome.timestamp));
    return transactions;
}

const createTransactions = () => {
    const { subscribe, set, update } = writable<FormattedPaymentTransaction[]>([]);

    return {
        subscribe,
        append: (newTx: FormattedPaymentTransaction) => update((txs) => sortTransactions([newTx, ...txs])),
        clear: () => set([]),
        set,
    };
}

interface Account {
    address: string;
    balance: number;
}

const createAccount = () => {
    const { subscribe, set, update } = writable<Account>({
        address: "rNiHeXMDTKSHqjfBx2QUV9U7GY7e6Ajwyf",
        balance: 0
    });

    return {
        subscribe,
        set,
    }
}

export const account = createAccount();
export const address = derived(account, $account => $account.address);
export const balance = derived(account, $account => $account.balance);
export const transactions = createTransactions();
