import { routes } from 'svelte-hash-router';
import App from './App.svelte';
import Home from "./routes/Home.svelte";
import LoggedIn from "./routes/LoggedIn.svelte";

routes.set({
	"/logged-in": LoggedIn,
	"/": Home,
})

const app = new App({
	target: document.body,
});

export default app;
