export interface ITransaction {
    address: string;
    id: string;
    outcome: {
        result: string;
        timestamp: string;
        fee: string;
        indexInLedger: number;
        ledgerVersion: number;
        deliveredAmount: {
            currency: string;
            value: string;
        };
        balanceChanges: {
            [key: string]: Array<{
                currency: string;
                value: string;
            }>;
        };
    };
    sequence: number;
    specification: {
        source: {
            address: string;
            maxAmount: {
                currency: string;
                value: string;
            };
        };
        destination: {
            address: string;
        };
    };
    type: string;
}
